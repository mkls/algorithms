n = int(input())
d = [0] * n
m = [[0] * 3] * n

d.extend([float('inf')] * 3)
m.extend([[float('inf')] * 3] * 3)

for i in range(n):
    *m[i], = map(int, input().split())


def calc_d(target: int):
    d[0] = 0

    for index in range(1, target + 1):
        d[index] = min(m[index - 1][0] + d[index - 1],
                       m[index - 2][1] + d[index - 2],
                       m[index - 3][2] + d[index - 3])
    return d[target]


print(calc_d(n))
