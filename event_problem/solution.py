class Event:
    def __init__(self, number: int, start: float, end: float, value: float):
        self.number = number
        self.start = start
        self.end = end
        self.value = value

    def __repr__(self):
        return f'<Event: {self.number}| {self.start}, {self.end}, {self.value}>'


event_count = int(input('Число мероприятий: '))
events = []

for i in range(1, event_count + 1):
    events.append(Event(i, *map(int, input().split())))

events.sort(key=lambda x: x.end)

s = [[0]] + [[] for _ in range(event_count - 1)]
d = [events[0].value] + [0] * (event_count - 1)

for i in range(1, event_count):
    j = map(lambda x: x.end <= events[i].start, events)

    m = 0
    k = None

    for g, less in enumerate(j):
        if less and d[g] >= m:
            m = d[g]
            k = g

    if k is None or d[k] + events[i - 1].value <= d[i - 1]:
        d[i] = events[i].value
        s[i] = [i]
    else:
        d[i] = d[k] + events[i].value
        s[i] = s[k] + [i]

print('Ответ:\n')
print('Ценность:', d[-1], '\nМероприятия:',
      sorted([events[i].number for i in s[-1]]))
