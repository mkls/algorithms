import os, heapq
from functools import partial
from typing import List


class Task:
    def __init__(self, name: str):
        self.name = name
        self.priority = 0
        self.prev_tasks = []
        self.prev_count = 0
        self.next_tasks = []
        self.next_count = 0
        self.mark = []

    def update_priority(self, priority: int):
        self.priority = priority

    def __lt__(self, other):
        ''.join(self.mark) < ''.join(other.mark)

    def __repr__(self):
        return f'Task<{self.name}, {self.priority}>'


def calc_priorities(tasks: List[Task]) -> None:
    p = 1
    end_tasks = [task for task in tasks if len(task.next_tasks) == 0]
    heapq.heapify(end_tasks)

    while len(end_tasks) > 0:
        current = heapq.heappop(end_tasks)
        current.update_priority(p)

        for prev in current.prev_tasks:
            tasks[prev - 1].mark.insert(0, str(p))
            tasks[prev - 1].next_count -= 1

            if tasks[prev - 1].next_count == 0:
                heapq.heappush(end_tasks, tasks[prev - 1])

        p += 1


def create_timetable(tasks: List[Task], executor_count: int) -> list:
    ttasks = [(-t.priority, t) for t in tasks]
    heapq.heapify(ttasks)

    *current_tasks, = filter(lambda x: x[1].prev_count == 0, ttasks)

    jobs = [[] for _ in range(executor_count)]

    while len(current_tasks) > 0:
        chosen = []
        for _ in range(executor_count):
            if len(current_tasks) == 0:
                break
            chosen.append(heapq.heappop(current_tasks))

        *chosen, = map(lambda x: x[1], chosen)
        chosen += [None] * (executor_count - len(chosen))

        for i, ch in enumerate(chosen):
            jobs[i].append(ch)

            if ch is not None:
                for n in ch.next_tasks:
                    tasks[n - 1].prev_count -= 1
                    if tasks[n - 1].prev_count == 0:
                        heapq.heappush(current_tasks,
                                       (-tasks[n - 1].priority, tasks[n - 1]))

    return jobs


def main():
    params = list(map(int, input().split()))
    task_count, executor_count, deps_count = params

    tasks = [Task(str(i)) for i in range(1, task_count + 1)]

    for _ in range(deps_count):
        dep, t = tuple(map(int, input().split('-')))

        tasks[t - 1].prev_tasks.append(dep)
        tasks[t - 1].prev_count += 1

        tasks[dep - 1].next_tasks.append(t)
        tasks[dep - 1].next_count += 1

    calc_priorities(tasks)

    for task in tasks:
        if task.priority == 0:
            print('Данный набор заданий невозможно выполнить :(')
            os.exit(1)

    jobs = create_timetable(tasks, executor_count)
    jobs = zip(*jobs)

    print('-' * 5 + 'Результат' + '-' * 5)
    for i, job in enumerate(jobs):
        print(i, '| ', end='')

        for j in job:
            print(j, end=' ')

        print()


if __name__ == '__main__':
    main()
