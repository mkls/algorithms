import copy
import heapq
from typing import List, Tuple


class Item:
    def __init__(self, index: int, value: int, weight: int):
        self.index = index
        self.value = value
        self.weight = weight

    def get(self):
        return self.value, self.weight

    def e_measure(self):
        return self.value / self.weight

    def __lt__(self, other):
        return self.e_measure() >= other.e_measure()

    def __repr__(self):
        return f'Item(i: {self.index}, v: {self.value}, w: {self.weight})'


class ItemSubset:
    def __init__(self, weight: float, value: float, indices: List[int],
                 level: int, capacity: float, next_item: Item):
        self.weight = weight
        self.value = value
        self.indices = copy.deepcopy(indices)
        self.level = level
        self.f_measure = ItemSubset.measure(weight, value, capacity, next_item)

    def __lt__(self, other):
        return self.f_measure >= other.f_measure

    def __repr__(self):
        s = "ItemSubset(w: {}, v: {}, idx: {}, f: {})"
        return s.format(self.weight, self.value, self.indices, self.f_measure)

    @staticmethod
    def measure(weight: float, value: float, capacity: float, next_item: Item):
        if next_item is None:
            e = 0
        else:
            e = next_item.e_measure()

        if weight <= capacity:
            return value + (capacity - weight) * e

        return -1


def knapsack(n: int, items: List[Item], weight_limit: int) -> List:
    max_value = 0
    weight = 0
    value = 0

    sorted_items = sorted(items)
    # print(*sorted_items, '\n')

    heap = []
    heapq.heappush(heap, ItemSubset(0, 0, [], -1, weight_limit, None))

    while True:
        cur = heapq.heappop(heap)
        level = cur.level + 1

        if level == len(sorted_items):
            break

        item = sorted_items[level]
        next_item = sorted_items[level + 1] if level + 1 < len(sorted_items)\
            else None

        heapq.heappush(
            heap,
            ItemSubset(cur.weight, cur.value, cur.indices, level, weight_limit,
                       next_item))
        heapq.heappush(
            heap,
            ItemSubset(cur.weight + item.weight, cur.value + item.value,
                       cur.indices + [item.index], level, weight_limit,
                       next_item))

        # print(f'Heap on level {level}:', *heap, '\n', sep='\n')

    solution = [items[i] for i in cur.indices]

    return solution


if __name__ == '__main__':
    weights = list(
        map(int,
            input('Item weights (space-separated):\n> ').split()))
    values = list(map(int,
                      input('Item values (space-separated):\n> ').split()))

    items = map(lambda x: Item(x[0], x[1][0], x[1][1]),
                enumerate(zip(values, weights)))
    items = list(items)
    # items = list(map(list, zip([0] * len(values), items)))

    weight_limit = int(input('Weight limit:\n> '))
    solution = knapsack(len(items), items, weight_limit)
    solution = sorted(list(map(lambda x: x.index + 1, solution)))

    print(f'Solution: {solution}')
    # print(*solution, sep='\n')
