#include <iostream>
#include <string>

using std::cin;
using std::cout;
using std::string;

int main(int argc, char **argv) {
    size_t j = 0;
    string sequence1, sequence2;

    cin >> sequence1 >> sequence2;

    for (size_t i = 0; i < sequence2.length() && j < sequence1.length(); i++) {
        if (sequence2[i] == sequence1[j]) {
            j++;
        }
    }

    if (j == sequence1.length()) {
        cout << "YES";
    } else {
        cout << "NO";
    }
    cout << std::endl;

    return 0;
}
