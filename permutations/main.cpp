// K-permutation
#include <algorithm>
#include <iostream>
// #include <fstream>
#include <vector>

using std::abs;
using std::endl;
// using std::ifstream;
// using std::ofstream;
using std::cin;
using std::cout;
using std::reverse;
using std::swap;
using std::vector;

template<typename T>
bool check_vector(vector<T> &vec, int k) {
    bool skip = false;

    for (int i = 0; i < int(vec.size()) - 1; i++) {
        if (abs(vec[i] - vec[i + 1]) > k) {
            skip = true;
            break;
        }
    }

    return skip;
}

// 1 2 3, 1 3 2, 2 1 3, 2 3 1, 3 1 2, 3 2 1

int main(int argc, char** argv) {
    int n, k;
    size_t counter = 0;

    cin >> n >> k;
    //auto in_file = ifstream("INPUT.txt");
    // in_file >> n >> k;
    // in_file.close();

    auto a = vector<int>(n);
    for (int i = 0; i < n; i++) {
        a[i] = i + 1;
    }

    while (true) {
        if (!check_vector(a, k)) {
            counter++;
        }

        auto g = n - 2;

        while (g > -1 && a[g] > a[g + 1]) {
            g--;
        }

        if (g == -1) {
            break;
        }

        auto j = n - 1;

        while (a[j] < a[g]) {
            j--;
        }

        swap(a[j], a[g]);
        reverse(a.begin() + g + 1, a.end());
    }

    cout << counter;
    // auto out_file = ofstream("OUTPUT.txt");
    // out_file << counter << endl;
    // out_file.close();

    return 0;
}
