#include <iostream>
#include <vector>

using std::vector;
using std::cin;
using std::cout;

int main() {
    size_t size, steps;
    cin >> size >> steps;

    vector<vector<char> > field(2 + size, vector<char>(2 + size, '1'));

    for (size_t i = 1; i <= size; i++) {
        for (size_t j = 1; j <= size; j++) {
            cin >> field[i][j];
        }
    }

    vector<vector<vector<int> > > count(
        steps + 1,
        vector<vector<int> >(
            size + 2,
            vector<int> (
                size + 2,
                0
            )
        )
    );

    count[0][1][1] = 1;

    for (size_t k = 1; k <= steps; k++) {
        for (size_t i = 1; i <= size; i++) {
            for (size_t j = 1; j <= size; j++) {
                if (field[i][j] == '0') {
                    auto up = count[k - 1][i - 1][j];
                    auto left = count[k - 1][i][j - 1];
                    auto right = count[k - 1][i][j + 1];
                    auto down = count[k - 1][i + 1][j];
                    count[k][i][j] = up + down + left + right;
                }
            }
        }
    }

    cout << count[steps][size][size] << "\n";

    return 0;
}
