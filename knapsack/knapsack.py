import copy
from typing import List, Tuple


class Item:
    def __init__(self, value: int, weight: int):
        self.value = value
        self.weight = weight

    def get(self):
        return self.value, self.weight

    def __repr__(self):
        return f'Item(v: {self.value}, w: {self.weight})'


def knapsack(n: int, items: List[Tuple[int, Item]], weight_limit: int) -> List:
    b = list(range(n + 2))

    max_value = 0
    solution = []

    weight = 0
    value = 0

    i = 0

    while i < n + 1 and b[0] < n:
        item = items[b[0]]
        v, w = item[1].get()

        if item[0] == 1:
            item[0] = 0
            weight -= w
            value -= v
        else:
            item[0] = 1
            weight += w
            value += v

        if weight_limit >= weight and value >= max_value:
            solution = copy.deepcopy(items)
            max_value = value

        i = b[0]
        b[0] = 0
        b[i] = b[i + 1]
        b[i + 1] = i + 1

    return solution


if __name__ == '__main__':
    weights = list(
        map(int,
            input('Item weights (space-separated):\n> ').split()))
    values = list(map(int,
                      input('Item values (space-separated):\n> ').split()))

    items = map(lambda x: Item(x[0], x[1]), zip(values, weights))
    items = list(map(list, zip([0] * len(values), items)))

    weight_limit = int(input('Weight limit:\n> '))
    solution = knapsack(len(items), items, weight_limit)

    print(*solution, sep='\n')
