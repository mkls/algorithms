﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using HuffmanFileWork;

namespace Huffman
{
    class HuffmanTree
    {
        public char ch { get; private set; }
        public double freq { get; private set; }
        public bool isTerminal { get; private set; }
        public HuffmanTree left { get; private set; }
        public HuffmanTree right { get; private set; }
        public HuffmanTree(char c, double frequency)
        {
            ch = c;
            freq = frequency;
            isTerminal = true;
            left = right = null;
        }
        public HuffmanTree(HuffmanTree l, HuffmanTree r)
        {
            freq = l.freq + r.freq;
            isTerminal = false;
            left = l;
            right = r;
        }

        public void Traverse(Dictionary<char, string> table, String code = "")
        {
            if (this.isTerminal)
            {
                table.Add(this.ch, code);
            }
            else
            {
                this.right.Traverse(table, code + "1");
                this.left.Traverse(table, code + "0");
            }
        }
    }
    class HuffmanTreeCompare : IComparer<HuffmanTree>
    {
        public int Compare(HuffmanTree x, HuffmanTree y)
        {
            return x.freq.CompareTo(y.freq);
        }
    }
    class HuffmanInfo
    {
        HuffmanTree Tree; // дерево кода Хаффмана, потребуется для распаковки
        Dictionary<char, string> Table; // словарь, хранящий коды всех символов, будет удобен для сжатия
        public HuffmanInfo(string fileName)
        {
            string line;
            StreamReader sr = new StreamReader(fileName, Encoding.Unicode);
            var leaves = new List<HuffmanTree>();

            // считать информацию о частотах символов
            while ((line = sr.ReadLine()) != null)
            {
                if (line.Length == 0)
                {
                    var newLineFreq = float.Parse(sr.ReadLine().Replace(',', '.'));
                    var emptyNode = new HuffmanTree('\n', newLineFreq);
                    leaves.Add(emptyNode);
                }
                else
                {
                    var ch = line[0];
                    var freq = float.Parse(line.Substring(2).Replace(',', '.'));
                    var node = new HuffmanTree(ch, freq);
                    leaves.Add(node);
                }
            }
            sr.Close();

            // Sort by frequencies
            leaves.Add(new HuffmanTree('\0', 0.0));
            // var stack = new Stack<HuffmanTree>(leaves);

            while (leaves.Count > 1)
            {
                // leaves.Sort((x, y) => x.freq.CompareTo(y.freq));
                var first = leaves[0];
                var second = leaves[1];

                leaves.RemoveAt(0);
                leaves.RemoveAt(0);

                var newNode = new HuffmanTree(second, first);
                var index = leaves.BinarySearch(0, leaves.Count, newNode, new HuffmanTreeCompare());
                if (index < 0)
                    index = ~index;
                leaves.Insert(index, newNode);
            }

            Tree = leaves[0]; // The only item left is Huffman tree root
            Table = new Dictionary<char, string>();
            Tree.Traverse(Table);
        }
        public void Compress(string inpFile, string outFile)
        {
            var sr = new StreamReader(inpFile, Encoding.Unicode);
            var sw = new ArchWriter(outFile); //нужна побитовая запись, поэтому использовать StreamWriter напрямую нельзя
            string line;
            while ((line = sr.ReadLine()) != null)
            {
                foreach (char c in line)
                {
                    sw.WriteWord(Table[c]);
                }
                sw.WriteWord(Table['\n']);
            }
            sr.Close();
            sw.WriteWord(Table['\0']); // записываем признак конца файла
            // sw.WriteWord("\0");
            // sw.WriteBit(0);
            sw.Finish();
        }
        public void Decompress(string archFile, string txtFile)
        {
            var sr = new ArchReader(archFile);
            var sw = new StreamWriter(txtFile, false, Encoding.Unicode);
            byte curBit;

            var currentNode = Tree;

            while (sr.ReadBit(out curBit))
            {
                if (curBit == 0)
                    currentNode = currentNode.left;
                else
                    currentNode = currentNode.right;

                if (currentNode.isTerminal)
                {
                    if (currentNode.ch == '\0')
                        break;
                    sw.Write(currentNode.ch);
                    currentNode = Tree;
                }
            }
            sr.Finish();
            sw.Close();
        }
    }

    class Huffman
    {
        static void Main(string[] args)
        {
            if (!File.Exists("freq.txt"))
            {
                Console.WriteLine("Не найден файл с частотами символов!");
                return;
            }
            if (args.Length == 0)
            {
                var hi = new HuffmanInfo("freq.txt");
                hi.Compress("etalon.txt", "etalon.arc");
                hi.Decompress("etalon.arc", "etalon_dec.txt");
                return;
            }
            if (args.Length != 3 || args[0] != "zip" && args[0] != "unzip")
            {
                Console.WriteLine("Синтаксис:");
                Console.WriteLine("Huffman.exe zip <имя исходного файла> <имя файла для архива>");
                Console.WriteLine("либо");
                Console.WriteLine("Huffman.exe unzip <имя файла с архивом> <имя файла для текста>");
                Console.WriteLine("Пример:");
                Console.WriteLine("Huffman.exe zip text.txt text.arc");
                return;
            }
            var HI = new HuffmanInfo("freq.txt");
            if (args[0] == "zip")
            {
                if (!File.Exists(args[1]))
                {
                    Console.WriteLine("Не найден файл с исходным текстом!");
                    return;
                }
                HI.Compress(args[1], args[2]);
            }
            else
            {
                if (!File.Exists(args[1]))
                {
                    Console.WriteLine("Не найден файл с архивом!");
                    return;
                }
                HI.Decompress(args[1], args[2]);
            }
            Console.WriteLine("Операция успешно завершена!");
        }
    }
}
