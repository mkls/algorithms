import subprocess
from tree import Tree


def main():
    values = [
        4, 3, 10, 2, 9, 42, 33, 11, 5, 555, 666, 777, 1000, 750, 650, 1024
    ]

    tree = Tree()
    for i, v in enumerate(values):
        tree.insert(v)
        print(tree.dot_repr())

        with open(f'graph{i}.svg', "w") as f:
            subprocess.run(['dot', '-Tsvg'],
                           stdout=f,
                           input=tree.dot_repr(),
                           encoding='utf-8')

    # print(tree.find(42))


if __name__ == '__main__':
    main()
