from node import Node


class Tree:
    def __init__(self):
        self.root = None

    def dot_repr(self):
        g = "digraph g {\n"
        g += 'node [shape = record,height=.1];\n'

        i = [0]
        _, s = self.root.dot_repr(i)
        g += s + "}\n"

        return g

    def insert(self, item):
        if self.root is None:
            self.root = Node(item)
        else:
            self.root.insert(Node(item))
            while self.root.parent:
                self.root = self.root.parent
        return True

    def find(self, item):
        return self.root.find(item)

    def remove(self, item):
        if self.root is None:
            return False

        self.root.remove(item)
