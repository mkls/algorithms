from typing import Any, List, Tuple


class Node:
    def __init__(self, value: Any = None, parent=None):
        self.values = [value]
        self.parent = parent
        self.children = []

    def __lt__(self, node) -> bool:
        return self.values[0] < node.values[0]

    def __str__(self) -> str:
        return f"Node[{self.values}]"

    def is_leaf(self) -> bool:
        return len(self.children) == 0

    def add(self, new_node) -> None:
        for child in new_node.children:
            child.parent = self

        self.values.extend(new_node.values)
        self.values.sort()
        self.children.extend(new_node.children)

        if len(self.children) > 1:
            self.children.sort()

        if len(self.values) > 2:
            self.split()

    def insert(self, new_node) -> None:
        if self.is_leaf():
            self.add(new_node)

        elif new_node.values[0] > self.values[-1]:
            self.children[-1].insert(new_node)
        else:
            for i, _ in enumerate(self.values):
                if new_node.values[0] < self.values[i]:
                    self.children[i].insert(new_node)
                    break

    def split(self) -> None:
        left_child = Node(self.values[0], self)
        right_child = Node(self.values[2], self)

        if self.children:
            self.children[0].parent = left_child
            self.children[1].parent = left_child
            self.children[2].parent = right_child
            self.children[3].parent = right_child

            left_child.children = [self.children[0], self.children[1]]
            right_child.children = [self.children[2], self.children[3]]

        self.children = [left_child]
        self.children.append(right_child)
        self.values = [self.values[1]]

        if self.parent:
            if self in self.parent.children:
                self.parent.children.remove(self)
            self.parent.add(self)
        else:
            left_child.parent = self
            right_child.parent = self

    def find(self, item):
        if item in self.values:
            return self
        elif self.is_leaf():
            return None
        elif item > self.values[-1]:
            return self.children[-1].find(item)
        else:
            for i in range(len(self.values)):
                if item < self.values[i]:
                    return self.children[i].find(item)

    def remove(self, value):
        target_node = self.find(value)
        if target_node is None:
            return False

        if target_node.is_leaf():
            target_node.values.remove(value)

            if len(target_node.values) == 0:
                if target_node.parent is None:
                    return None
                else:
                    p = target_node.parent
                    pp = p.parent
                    rmin = p.children[-1].search_min()

                    if pp is None:
                        rmin.parent.children.remove(rmin)
                        rmin.parent = None
                        rmin.children.append(p)

    def search_min(self):
        if self.is_leaf():
            return self
        return self.children[0].search_min()

    def dot_repr(self, index: List[int]) -> Tuple[int, str]:
        base = f'node{index[0]}'
        current = index[0]

        index[0] += 1

        if len(self.values) == 1:
            base += f'[label="<f0>|{self.values[0]}|<f1>"];\n'
        else:
            base += '[label="<f0>|{}|<f1>|{}|<f2>"];\n'.format(*self.values)

        for c, i in zip(self.children, range(len(self.children))):
            next, children = c.dot_repr(index)
            base += children
            base += f'"node{current}":f{i} -> "node{next}";\n'

        return current, base
